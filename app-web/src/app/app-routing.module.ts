import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentComponent } from './components/appointment/appointment.component';
import { DialogAppointmentComponent } from './components/appointment/dialog-appointment/dialog-appointment.component';
import { DialogEditAppointmentComponent } from './components/appointment/dialog-edit-appointment/dialog-edit-appointment.component';
import { ClientComponent } from './components/client/client.component';
import { DialogClientComponent } from './components/client/dialog-client/dialog-client.component';
import { DialogEditClientComponent } from './components/client/dialog-edit-client/dialog-edit-client.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { DialogEditMedicalOrderComponent } from './components/medical-order/dialog-edit-medical-order/dialog-edit-medical-order.component';
import { DialogMedicalOrderComponet } from './components/medical-order/dialog-medical-order/dialog-medical-order.component';
import { MedicalOrderComponent } from './components/medical-order/medical-order.component';
import { DialogEditPetComponent } from './components/pet/dialog-edit-pet/dialog-edit-pet.component';
import { DialogPetComponent } from './components/pet/dialog-pet/dialog-pet.component';
import { PetComponent } from './components/pet/pet.component';
import { ProductComponent } from './components/product/product.component';
import { RegisterComponent } from './components/register/register.component';
import { DialogEditVeterinaryComponent } from './components/veterinary/dialog-edit-veterinary/dialog-edit-veterinary.component';
import { DialogVeterinaryComponent } from './components/veterinary/dialog-veterinary/dialog-veterinary.component';
import { VeterinaryComponent } from './components/veterinary/veterinary.component';

const routes: Routes = [
  { path:'home', component: HomeComponent},
  { path:'product', component: ProductComponent},
  { path:'client', component: ClientComponent},
  { path:'veterinary', component: VeterinaryComponent},
  { path:'pet', component: PetComponent},
  { path:'medical-order', component: MedicalOrderComponent},
  { path:'appointment', component: AppointmentComponent},
  { path:'login', component: LoginComponent},
  { path:'register', component: RegisterComponent},
  { path:'**', redirectTo: '/home', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

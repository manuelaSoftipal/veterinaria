import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgbAlertConfig, NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { NgIf } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home.component';
import { ProductComponent } from './components/product/product.component';
import { ClientComponent } from './components/client/client.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import { DialogClientComponent } from './components/client/dialog-client/dialog-client.component';
import { HttpClientModule } from '@angular/common/http';
import { DialogEditClientComponent } from './components/client/dialog-edit-client/dialog-edit-client.component';
import { DialogEditVeterinaryComponent } from './components/veterinary/dialog-edit-veterinary/dialog-edit-veterinary.component';
import { DialogVeterinaryComponent } from './components/veterinary/dialog-veterinary/dialog-veterinary.component';
import { VeterinaryComponent } from './components/veterinary/veterinary.component';
import { PetComponent } from './components/pet/pet.component';
import { DialogPetComponent } from './components/pet/dialog-pet/dialog-pet.component';
import { DialogEditPetComponent } from './components/pet/dialog-edit-pet/dialog-edit-pet.component';
import { MedicalOrderComponent } from './components/medical-order/medical-order.component';
import { DialogMedicalOrderComponet } from './components/medical-order/dialog-medical-order/dialog-medical-order.component';
import { DialogEditMedicalOrderComponent } from './components/medical-order/dialog-edit-medical-order/dialog-edit-medical-order.component';
import { AppointmentComponent } from './components/appointment/appointment.component';
import { DialogAppointmentComponent } from './components/appointment/dialog-appointment/dialog-appointment.component';
import { DialogEditAppointmentComponent } from './components/appointment/dialog-edit-appointment/dialog-edit-appointment.component';
import { LoginComponent } from './components/login/login.component';
import { LoginC } from './class/login.class';
import { RegisterComponent } from './components/register/register.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProductComponent,
    ClientComponent,
    VeterinaryComponent,
    DialogClientComponent,
    DialogEditClientComponent,
    DialogEditVeterinaryComponent,
    DialogVeterinaryComponent,
    PetComponent,
    DialogPetComponent,
    DialogEditPetComponent,
    MedicalOrderComponent,
    DialogMedicalOrderComponet,
    DialogEditMedicalOrderComponent,
    AppointmentComponent,
    DialogAppointmentComponent,
    DialogEditAppointmentComponent,
    LoginComponent,
    RegisterComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatDialogModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgbToastModule,
    NgIf,
  ],
  entryComponents:[DialogClientComponent],
  providers: [LoginC],
  bootstrap: [AppComponent],
})
export class AppModule { }

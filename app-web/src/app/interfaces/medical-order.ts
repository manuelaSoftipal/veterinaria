import { EmailValidator } from "@angular/forms";

export interface MedicalOrder{
    medicalOrderId: number;
    veterinaryId: number;
    petId: number;
    medicines: string;
    quantity: string;
    durationTime: string;
}
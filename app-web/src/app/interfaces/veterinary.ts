import { EmailValidator } from "@angular/forms";

export interface Veterinary{
    veterinaryId: number;
    firstName: string;
    lastName: string;
    email: string;
    adress: string;
    specialty?: string;
    password: string;
}
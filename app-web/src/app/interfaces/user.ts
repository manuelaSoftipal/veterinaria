import { EmailValidator } from "@angular/forms";

export interface Users{
    clientId:number;
    Rol: string;
    FirstName: string;
    LastName:string;
    Email: string;
    Password: string;
}
import { EmailValidator } from "@angular/forms";

export interface Pet{
    petId: number;
    name: string;
    age: number;
    breed: string;
    color: string;
    image: string;
    clientId: number;
}
import { EmailValidator } from "@angular/forms";

export interface Response{
    response: string;
    role:string;
}
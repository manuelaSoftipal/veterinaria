import { EmailValidator } from "@angular/forms";

export interface Client{
    clientId:number;
    firstName: string;
    lastName: string;
    adress:string;
    email: string;
    password: string;
}
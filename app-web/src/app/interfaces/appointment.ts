import { EmailValidator } from "@angular/forms";

export interface Appointment{
    appointmentId: number;
    date: Date;
    petId: number;
    veterinaryId: number;
}
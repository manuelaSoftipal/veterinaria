import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Users } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/users/"
  constructor( private http: HttpClient) { }

  getListUser(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl);
  }


  deleteUser(id: number): Observable<any>{
    return this.http.delete(this.myAppUrl + this.myApiUrl + id);
  }

  saveUser(user: Users): Observable<any>{
    return this.http.post(this.myAppUrl + this.myApiUrl, user);
  }

  getUser(id: number): Observable<any>{
    return this.http.get(this.myAppUrl + this.myApiUrl + id);
  }

  updateUser(id: number, user: Users): Observable<any>{
    return this.http.put(this.myAppUrl + this.myApiUrl+ id, user);
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from '../interfaces/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  
  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/client/"
  constructor( private http: HttpClient) { }

  getListClient(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl);
  }


  deleteClient(id: number): Observable<any>{
    return this.http.delete(this.myAppUrl + this.myApiUrl + id);
  }

  saveClient(client: Client): Observable<any>{
    return this.http.post(this.myAppUrl + this.myApiUrl, client);
  }

  getClient(id: number): Observable<any>{
    return this.http.get(this.myAppUrl + this.myApiUrl + id);
  }

  updateClient(id: number, client: Client): Observable<any>{
    return this.http.put(this.myAppUrl + this.myApiUrl+ id, client);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pet } from '../interfaces/pet';


@Injectable({
  providedIn: 'root'
})
export class PetService {
  
  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/pet/"
  constructor( private http: HttpClient) { }

  getListPet(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl);
  }

  deletePet(id: number): Observable<any>{
    return this.http.delete(this.myAppUrl + this.myApiUrl + id);
  }

  savePet(pet: Pet): Observable<any>{
    return this.http.post(this.myAppUrl + this.myApiUrl, pet);
  }

  getPet(id: number): Observable<any>{
    return this.http.get(this.myAppUrl + this.myApiUrl + id);
  }

  updatePet(id: number, pet: Pet): Observable<any>{
    return this.http.put(this.myAppUrl + this.myApiUrl+ id, pet);
  }

  getListClient(): Observable<any> {
    return this.http.get("https://localhost:44309/api/client/");
  }
}

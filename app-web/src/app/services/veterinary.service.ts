import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Veterinary} from '../interfaces/veterinary';

@Injectable({
  providedIn: 'root'
})
export class VeterinaryService {
  
  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/veterinary/"
  constructor( private http: HttpClient) { }

  getListVeterinary(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl);
  }

  deleteVeterinary(id: number): Observable<any>{
    return this.http.delete(this.myAppUrl + this.myApiUrl + id);
  }

  saveVeterinary(veterinary: Veterinary): Observable<any>{
    return this.http.post(this.myAppUrl + this.myApiUrl, veterinary);
  }

  getVeterinary(id: number): Observable<any>{
    return this.http.get(this.myAppUrl + this.myApiUrl + id);
  }

  updateVeterinary(id: number, veterinary: Veterinary): Observable<any>{
    return this.http.put(this.myAppUrl + this.myApiUrl+ id, veterinary);
  }
}

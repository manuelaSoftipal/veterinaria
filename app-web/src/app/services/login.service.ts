import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginC } from '../class/login.class';
import { Client } from '../interfaces/client';
import { Login } from '../interfaces/login';
import { Subscription, Observable, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  sesionData: BehaviorSubject<LoginC> = new BehaviorSubject<LoginC>(new LoginC());

  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/login/"
  constructor(private http: HttpClient) { 
    // this.verifySession();
  }


  login(login: Login): Observable<any> {
    return this.http.post<Response>(this.myAppUrl + this.myApiUrl, login, { observe: 'response' });
  }


  data(loginC: LoginC): Boolean {
    let rol = localStorage.getItem('rol');

    if (!rol) {
      return false;
    } else {
      let datosString = JSON.stringify(loginC);
      // localStorage.setItem('rol', datosString);

      if(localStorage.getItem('isLoggedIn')){
        loginC.isLoggedIn = true;
        this.refresh(loginC);
        this.verifySession(loginC);
        return true

      }else{
        loginC.isLoggedIn = false;
        this.refresh(loginC);
        this.verifySession(loginC);
        return true
      }

    }

  }

  refresh(loginC: LoginC) {
    this.sesionData.next(loginC)
  }

  observable() {
    return this.sesionData.asObservable();
  }

  verifySession(loginC: LoginC){
    // let datos = localStorage.getItem("rol");
    if(loginC){
      let object : LoginC = JSON.parse(JSON.stringify(loginC));
      // object.isLoggedIn = true;
      this.refresh(object);
    }
  }

}





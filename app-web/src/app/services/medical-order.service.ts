import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MedicalOrder} from '../interfaces/medical-order';

@Injectable({
  providedIn: 'root'
})
export class MedicalOrderService {
  
  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/medicalorder/"
  constructor( private http: HttpClient) { }

  getListMedicalOrder(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl);
  }

  deleteMedicalOrder(id: number): Observable<any>{
    return this.http.delete(this.myAppUrl + this.myApiUrl + id);
  }

  saveMedicalOrder(medicalOrder: MedicalOrder): Observable<any>{
    return this.http.post(this.myAppUrl + this.myApiUrl, medicalOrder);
  }

  getMedicalOrder(id: number): Observable<any>{
    return this.http.get(this.myAppUrl + this.myApiUrl + id);
  }

  updateMedicalOrder(id: number, medicalOrder: MedicalOrder): Observable<any>{
    return this.http.put(this.myAppUrl + this.myApiUrl+ id, medicalOrder);
  }

  getListVeterinary(): Observable<any> {
    return this.http.get("https://localhost:44309/api/veterinary/");
  }

  getListPet(): Observable<any> {
    return this.http.get("https://localhost:44309/api/pet/");
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Appointment } from '../interfaces/appointment';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  
  private myAppUrl = "https://localhost:44309/";
  private myApiUrl = "api/appointment/"
  constructor( private http: HttpClient) { }

  getListAppointment(): Observable<any> {
    return this.http.get(this.myAppUrl + this.myApiUrl);
  }

  deleteAppointment(id: number): Observable<any>{
    return this.http.delete(this.myAppUrl + this.myApiUrl + id);
  }

  saveAppointment(appointment: Appointment): Observable<any>{
    return this.http.post(this.myAppUrl + this.myApiUrl, appointment);
  }

  getAppointment(id: number): Observable<any>{
    return this.http.get(this.myAppUrl + this.myApiUrl + id);
  }

  updateAppointment(id: number, appointment: Appointment): Observable<any>{
    return this.http.put(this.myAppUrl + this.myApiUrl+ id, appointment);
  }

  getListVeterinary(): Observable<any> {
    return this.http.get("https://localhost:44309/api/veterinary/");
  }

  getListPet(): Observable<any> {
    return this.http.get("https://localhost:44309/api/pet/");
  }
}

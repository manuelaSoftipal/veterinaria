import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MedicalOrder } from 'src/app/interfaces/medical-order';
import { Pet } from 'src/app/interfaces/pet';
import { Veterinary } from 'src/app/interfaces/veterinary';
import { MedicalOrderService } from 'src/app/services/medical-order.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-medical-order',
  templateUrl: './dialog-medical-order.component.html',
  styleUrls: ['./dialog-medical-order.component.scss']
})
export class DialogMedicalOrderComponet {
  createMedicalOrder: FormGroup;
  datos = this.data.datos.listMedicalOrder;
  listVeterinary: Veterinary[] = []
  listPet: Pet[] = []

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogMedicalOrderComponet>,
    private _medicalOrderService: MedicalOrderService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.createMedicalOrder = this.fb.group({
      medicalOrderId: [null, Validators.required],
      veterinaryId: [null, Validators.required],
      petId: [null, Validators.required],
      medicines: [null, Validators.required],
      quantity: [null, Validators.required],
      durationTime: [null, Validators.required],
    })
  }

  ngOnInit() {
    this.getVeterinay();
    this.getPet();
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  create() {
    const medicalOrder: MedicalOrder = {
      medicalOrderId: this.createMedicalOrder.get("medicalOrderId")?.value,
      veterinaryId: this.createMedicalOrder.get("veterinaryId")?.value,
      petId: this.createMedicalOrder.get("petId")?.value,
      medicines: this.createMedicalOrder.get("medicines")?.value,
      quantity: this.createMedicalOrder.get("quantity")?.value,
      durationTime: this.createMedicalOrder.get("durationTime")?.value,
    }
    console.log(medicalOrder);
    this._medicalOrderService.saveMedicalOrder(medicalOrder).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'La orden medica se creo correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible crear la orden medica correctamente',
      })
    })

  }

  getVeterinay() {
    this._medicalOrderService.getListVeterinary().subscribe(data => {
      console.log(data);
      this.listVeterinary = data;
    }, error => {
      console.log(error);
    })
  }

  getPet() {
    this._medicalOrderService.getListPet().subscribe(data => {
      console.log(data);
      this.listPet = data;
    }, error => {
      console.log(error);
    })
  }

}

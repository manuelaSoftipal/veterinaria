import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MedicalOrder } from 'src/app/interfaces/medical-order';
import { Pet } from 'src/app/interfaces/pet';
import { Veterinary } from 'src/app/interfaces/veterinary';
import { MedicalOrderService } from 'src/app/services/medical-order.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-edit-medical-order',
  templateUrl: './dialog-edit-medical-order.component.html',
  styleUrls: ['./dialog-edit-medical-order.component.scss']
})
export class DialogEditMedicalOrderComponent {

  editMedicalOrder: FormGroup;
  medicalOrder: MedicalOrder | any;
  listVeterinary: Veterinary[] = []
  listPet: Pet[] = []

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditMedicalOrderComponent>,
    private _medicalOrderService: MedicalOrderService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editMedicalOrder = this.fb.group({
      medicalOrderId: [null, Validators.required],
      veterinaryId: [null, Validators.required],
      petId: [null, Validators.required],
      medicines: [null, Validators.required],
      quantity: [null, Validators.required],
      durationTime: [null, Validators.required],
    })
  }

  ngOnInit() {
    this.get(this.data.datos.medicalOrderId);
    this.getVeterinay();
    this.getPet();
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  get(id: number) {
    
    this._medicalOrderService.getMedicalOrder(id).subscribe(data => {
      this.editMedicalOrder.patchValue({
        medicalOrderId: data.medicalOrderId,
        veterinaryId: data.veterinaryId,
        petId: data.petId,
        medicines: data.medicines,
        quantity: data.quantity,
        durationTime: data.durationTime,
      })
    }, error => {
      console.log(error);
    })
  }

  update(){
    const medicalOrder: MedicalOrder = {
      medicalOrderId: this.editMedicalOrder.get("medicalOrderId")?.value,
      veterinaryId: this.editMedicalOrder.get("veterinaryId")?.value,
      petId: this.editMedicalOrder.get("petId")?.value,
      medicines: this.editMedicalOrder.get("medicines")?.value,
      quantity: this.editMedicalOrder.get("quantity")?.value,
      durationTime: this.editMedicalOrder.get("durationTime")?.value,
    }

    this._medicalOrderService.updateMedicalOrder(this.data.datos.medicalOrderId,medicalOrder).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'La orden medica se ha editado correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible editar la orden medica correctamente',
      })
    })
  }

  getVeterinay() {
    this._medicalOrderService.getListVeterinary().subscribe(data => {
      console.log(data);
      this.listVeterinary = data;
    }, error => {
      console.log(error);
    })
  }

  getPet() {
    this._medicalOrderService.getListPet().subscribe(data => {
      console.log(data);
      this.listPet = data;
    }, error => {
      console.log(error);
    })
  }

}

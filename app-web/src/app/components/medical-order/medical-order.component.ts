import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MedicalOrderService } from 'src/app/services/medical-order.service';
import { MedicalOrder } from 'src/app/interfaces/medical-order';
import { DialogEditMedicalOrderComponent } from './dialog-edit-medical-order/dialog-edit-medical-order.component';
import { DialogMedicalOrderComponet } from './dialog-medical-order/dialog-medical-order.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medical-order',
  templateUrl: './medical-order.component.html',
  styleUrls: ['./medical-order.component.scss']
})
export class MedicalOrderComponent {
  constructor(public _matDialog: MatDialog,
    private _medicalOrderService: MedicalOrderService) { }

  ngOnInit(): void {
    this.getMedicalOrder();
  }

  pageActual:number = 1;
  dialogRef: any;
  listMedicalOrder: MedicalOrder[] = []

  dialogMedicalOrder(): void {

    this.dialogRef = this._matDialog.open(DialogMedicalOrderComponet, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listMedicalOrder: this.listMedicalOrder,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getMedicalOrder();
        }

      });
  }

  dialogEditMedicalOrder(id: number){
    this.dialogRef = this._matDialog.open(DialogEditMedicalOrderComponent, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listMedicalOrder: this.listMedicalOrder,
          medicalOrderId: id,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getMedicalOrder();
        }

      });
  }

  getMedicalOrder() {
    this._medicalOrderService.getListMedicalOrder().subscribe(data => {
      console.log(data);
      this.listMedicalOrder = data;
    }, error => {
      console.log(error);
    })
  }

  deleteMedicalOrder(id: number) {
    // console.log(id);
    Swal.fire({
      title: 'Seguro que desea eliminar esta orden medica?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Si',
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        this._medicalOrderService.deleteMedicalOrder(id).subscribe(data => {
          this.getMedicalOrder();
          Swal.fire({
            icon: 'success',
            title: '¡Excelente!',
            text: 'Su orden medica se elimino correctamente!',
          });
        }, error => {
          this.getMedicalOrder();
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No fue posible eliminar su orden medica',
          })
        })
      }else if (result.isDenied) {
        this.getMedicalOrder();
      }
    })



    
  }

  

}

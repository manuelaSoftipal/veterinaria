
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/services/login.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean = false;
  role = localStorage.getItem('rol');
  // isLoggedIn = localStorage.getItem('isLoggedIn');

  constructor(private _loginService: LoginService) { }

  suscription: Subscription = new Subscription();

  ngOnInit(): void {

    this.suscription = this._loginService.observable().subscribe(

      (datos) => { 

        if(localStorage.getItem('isLoggedIn') == 'true'){
          this.isLoggedIn = true;

        }else if(localStorage.getItem('isLoggedIn') == 'false' || null)
        this.isLoggedIn = false;
        
      }, (error) => { }
    );
  }


  destroySesion(){
    localStorage.removeItem('token');
    localStorage.removeItem('rol');
    localStorage.removeItem('isLoggedIn');
  }

}

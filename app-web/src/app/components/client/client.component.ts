
import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogClientComponent } from './dialog-client/dialog-client.component';
import { ClientService } from 'src/app/services/client.service';
import { DialogEditClientComponent } from './dialog-edit-client/dialog-edit-client.component';
import Swal from 'sweetalert2';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from 'src/app/interfaces/client';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  listClient: Client[] = [];

  constructor(public _matDialog: MatDialog,
    private _clientService: ClientService,
    private http: HttpClient) {
  }

  ngOnInit() {
    this.getClients();
  }

  dialogRef: any;

  pageActual: number = 1;

  dialogClients(): void {

    this.dialogRef = this._matDialog.open(DialogClientComponent, {
      panelClass: 'contact-form-dialog',
      data: {
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getClients();
        }

      });
  }

  dialogEditclients(id: number) {
    this.dialogRef = this._matDialog.open(DialogEditClientComponent, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listClient: this.listClient,
          clientId: id,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getClients();
        }

      });
  }

  // getClients() {

  //   let httpsHeaders: HttpHeaders = new HttpHeaders();
  //   const token = localStorage.getItem('token');
  //   console.log('get token', token);

  //   this._clientService.getListClient(token,httpsHeaders).subscribe(data => {
  //     console.log(data.body)
  //     this.listClient = data.body;
  //   }, error => {
  //     console.log(error);
  //   })
  // }


  // getClients() {

  //   let https: HttpHeaders = new HttpHeaders();
  //   const token = localStorage.getItem('token');


  //   https = https.append('Authorization', 'Bearer ' + token);

  //   console.log(https);
  //   console.log(https["lazyUpdate"][0].value);

  //   const saveToken = https["lazyUpdate"][0].value;

  //   return this.http.get<Client[]>("https://localhost:44309/api/client/", 
  //   {headers: saveToken}).subscribe(data => {

  //     console.log(data);
  //     // this.listClient = data;
  //   })

  // }

  getClients() {
    this._clientService.getListClient().subscribe(data => {
      console.log(data);
      this.listClient = data;
    }, error => {
      console.log(error);
    })
  }

  deleteClients(id: number) {

    Swal.fire({
      title: 'Seguro que desea eliminar este cliente?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Si',
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        this._clientService.deleteClient(id).subscribe(data => {
          this.getClients();
          Swal.fire({
            icon: 'success',
            title: '¡Excelente!',
            text: 'El cliente se elimino correctamente!',
          });
        }, error => {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No fue posible eliminar su cliente',
          })
        })
      } else if (result.isDenied) {
        this.getClients();
      }
    })


  }


}

import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Client } from 'src/app/interfaces/client';
import { ClientService } from 'src/app/services/client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-edit-client',
  templateUrl: './dialog-edit-client.component.html',
  styleUrls: ['./dialog-edit-client.component.scss']
})
export class DialogEditClientComponent {

  editClient: FormGroup;
  client: Client | any;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditClientComponent>,
    private _clientService: ClientService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editClient = this.fb.group({
      clientId: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      adress: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  ngOnInit() {
    this.get(this.data.datos.clientId);
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  get(id: number) {
    
    this._clientService.getClient(id).subscribe(data => {
      this.editClient.patchValue({
        clientId: data.clientId,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        adress: data.adress,
        password: data.password,
      })
    }, error => {
      console.log(error);
    })
  }

  update(){
    const client: Client = {
      clientId: this.editClient.get("clientId")?.value,
      firstName: this.editClient.get("firstName")?.value,
      lastName: this.editClient.get("lastName")?.value,
      email: this.editClient.get("email")?.value,
      adress: this.editClient.get("adress")?.value,
      password: this.editClient.get("password")?.value,
    }

    this._clientService.updateClient(this.data.datos.clientId,client).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'El cliente se ha editado correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible editar el cliente correctamente',
      })
    })
  }

}

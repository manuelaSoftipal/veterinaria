import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Client } from 'src/app/interfaces/client'; 
import { ClientService } from 'src/app/services/client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-client',
  templateUrl: './dialog-client.component.html',
  styleUrls: ['./dialog-client.component.scss']
})
export class DialogClientComponent {
  createClient:FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogClientComponent>,
    private _clientService: ClientService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) {}
  ){
    this.createClient = this.fb.group({
      clientId: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      adress: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  ngOnInit(){}

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  create(){    
    const client: Client = {
      clientId: this.createClient.get("clientId")?.value,
      firstName: this.createClient.get("firstName")?.value,
      lastName: this.createClient.get("lastName")?.value,
      email: this.createClient.get("email")?.value,
      adress: this.createClient.get("adress")?.value,
      password: this.createClient.get("password")?.value,
    }
    console.log(client);
    this._clientService.saveClient(client).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'El cliente se creo correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible crear el cliente correctamente',
      })
    })

  }
  
}

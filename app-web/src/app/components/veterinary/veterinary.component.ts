import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogVeterinaryComponent } from './dialog-veterinary/dialog-veterinary.component';
import { DialogEditVeterinaryComponent } from './dialog-edit-veterinary/dialog-edit-veterinary.component';
import { VeterinaryService } from 'src/app/services/veterinary.service';
import { Veterinary } from 'src/app/interfaces/veterinary';

@Component({
  selector: 'app-veterinary',
  templateUrl: './veterinary.component.html',
  styleUrls: ['./veterinary.component.scss']
})
export class VeterinaryComponent {
  constructor(public _matDialog: MatDialog,
    private _veterinaryService: VeterinaryService) { }

  ngOnInit(): void {
    this.getVeterinarians();
  }

  pageActual:number =1;
  dialogRef: any;
  listVeterinary: Veterinary[] = []

  dialogVeterinarians(): void {

    this.dialogRef = this._matDialog.open(DialogVeterinaryComponent, {
      panelClass: 'contact-form-dialog',
      data: {
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getVeterinarians();
        }

      });
  }

  dialogEditVeterinarians(id: number){
    this.dialogRef = this._matDialog.open(DialogEditVeterinaryComponent, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listVeterinary: this.listVeterinary,
          veterinaryId: id,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getVeterinarians();
        }

      });
  }

  getVeterinarians() {
    this._veterinaryService.getListVeterinary().subscribe(data => {
      console.log(data);
      this.listVeterinary = data;
    }, error => {
      console.log(error);
    })
  }

  deleteVeterinarians(id: number) {
    console.log(id);
    this._veterinaryService.deleteVeterinary(id).subscribe(data => {
      this.getVeterinarians();
    }, error => {
      console.log(error);
    })
  }

  

}

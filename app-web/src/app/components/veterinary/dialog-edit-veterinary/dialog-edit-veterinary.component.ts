import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Veterinary } from 'src/app/interfaces/veterinary';

import { VeterinaryService } from 'src/app/services/veterinary.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-edit-veterinary',
  templateUrl: './dialog-edit-veterinary.component.html',
  styleUrls: ['./dialog-edit-veterinary.component.scss']
})
export class DialogEditVeterinaryComponent {

  editVeterinary: FormGroup;
  veterinary: Veterinary | any;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditVeterinaryComponent>,
    private _veterinaryService: VeterinaryService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editVeterinary = this.fb.group({
      veterinaryId: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      adress: [null, Validators.required],
      specialty: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  ngOnInit() {
    this.get(this.data.datos.veterinaryId);
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  get(id: number) {
    
    this._veterinaryService.getVeterinary(id).subscribe(data => {
      this.editVeterinary.patchValue({
        veterinaryId: data.veterinaryId,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        adress: data.adress,
        specialty: data.specialty,
        password: data.password,
      })
    }, error => {
      console.log(error);
    })
  }

  update(){
    const veterinary: Veterinary = {
      veterinaryId: this.editVeterinary.get("veterinaryId")?.value,
      firstName: this.editVeterinary.get("firstName")?.value,
      lastName: this.editVeterinary.get("lastName")?.value,
      email: this.editVeterinary.get("email")?.value,
      adress: this.editVeterinary.get("adress")?.value,
      specialty: this.editVeterinary.get("specialty")?.value,
      password: this.editVeterinary.get("password")?.value,
    }

    this._veterinaryService.updateVeterinary(this.data.datos.veterinaryId,veterinary).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'El veterinario se ha editado correctamente!',
      });
    }, error => {
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible editar el veterinario correctamente',
      })
    })
  }

}

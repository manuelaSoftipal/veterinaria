import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Veterinary } from 'src/app/interfaces/veterinary'; 
import { VeterinaryService } from 'src/app/services/veterinary.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-veterinary',
  templateUrl: './dialog-veterinary.component.html',
  styleUrls: ['./dialog-veterinary.component.scss']
})
export class DialogVeterinaryComponent {
  createVeterinary:FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogVeterinaryComponent>,
    private _veterinaryService: VeterinaryService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) {}
  ){
    this.createVeterinary = this.fb.group({
      veterinaryId: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      adress: [null, Validators.required],
      specialty: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  ngOnInit(){}

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  create(){  
    const veterinary: Veterinary = {
      veterinaryId: this.createVeterinary.get("veterinaryId")?.value,
      firstName: this.createVeterinary.get("firstName")?.value,
      lastName: this.createVeterinary.get("lastName")?.value,
      email: this.createVeterinary.get("email")?.value,
      adress: this.createVeterinary.get("adress")?.value,
      specialty: this.createVeterinary.get("specialty")?.value,
      password: this.createVeterinary.get("password")?.value,
    }
    console.log(veterinary);
    this._veterinaryService.saveVeterinary(veterinary).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'El veterinario se ha creo correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible crear el veterinario  correctamente',
      })
    })

  }
  
}

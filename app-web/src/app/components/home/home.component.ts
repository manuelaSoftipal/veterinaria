import { Component } from '@angular/core';
import { NgbCarouselConfig, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { NgIf } from '@angular/common';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent {
  info: any[] = [{
    img: '/assets/img2.jpg'
  },
  {
    img: '/assets/img1.jpg'
  },
  {
    img: '/assets/img3.jpg'
  }];

  pets: any[] = [{
    name: 'Perros', 
    img: '/assets/dog.png', 
    cares: 'Cuidar a un perro implica compromiso y atención. En general, es importante favorecer su actividad física y estimular su socialización. También es fundamental ofrecerle cuidados y revisiones veterinarias para controlar su estado de salud y mejorar su calidad de vida.', 
    feeding: 'Ofrecerles una alimentación saludable para ellos',
  },
  {
    name: 'Gatos', 
    img: '/assets/cat.png', 
    cares: 'Entre los cuidados de un gato son imprescindibles la vacunación y desparasitación siguiendo las indicaciones del veterinario. Debes tener una bandeja sanitaria y limpiar diariamente las deposiciones', 
    feeding: 'Debes dejarle comida todo el día en un lugar que sea de fácil acceso para él y donde no haya insectos o roedores',
  },
  {
    name: 'Hamster', 
    img: '/assets/hamster.png', 
    cares: 'Dedícale un rato de tu tiempo todos los días. Respeta sus horas de sueño: son nocturnos, así que duermen durante el día. No lo bañes nunca; los hámsters son muy higiénicos y se limpian solos. Limpia su jaula una vez a la semana con agua y jabón.', 
    feeding: 'Algunos de los alimentos que comen los hamster son los cereales, las semillas, las frutas y las verduras.',
  },
  {
    name: 'Peces', 
    img: '/assets/fish.png', 
    cares: 'Tenlo claro y mantenlos a la temperatura adecuada. No dejes que estén siempre a oscuras, los peces también necesitan recibir luz y será tu labor proporcionársela', 
    feeding: 'Pueden ser detrito*, bacterias*, plancton*, gusanos, insectos, caracoles, plantas acuáticas y peces',
  }]

  constructor(private _config: NgbCarouselConfig) { };
   
  ngOnInit(){}
}
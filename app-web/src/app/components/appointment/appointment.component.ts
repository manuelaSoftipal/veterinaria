import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogAppointmentComponent } from './dialog-appointment/dialog-appointment.component';
import { DialogEditAppointmentComponent} from './dialog-edit-appointment/dialog-edit-appointment.component';
import { AppointmentService } from 'src/app/services/appointment.service';

import Swal from 'sweetalert2';
import { Appointment } from 'src/app/interfaces/appointment';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent {
  constructor(public _matDialog: MatDialog,
    private _appointmentService: AppointmentService) { }

  ngOnInit(): void {
    this.getAppointments();
  }

  pageActual:number =1;
  dialogRef: any;
  listAppointment: Appointment[] = []

  dialogAppointments(): void {

    this.dialogRef = this._matDialog.open(DialogAppointmentComponent, {
      panelClass: 'contact-form-dialog',
      data: {
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getAppointments();
        }

      });
  }

  dialogEditAppointments(id: number){
    this.dialogRef = this._matDialog.open(DialogEditAppointmentComponent, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listAppointment: this.listAppointment,
          appointmentId: id,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getAppointments();
        }

      });
  }

  getAppointments() {
    this._appointmentService.getListAppointment().subscribe(data => {
      console.log(data);
      this.listAppointment = data;
    }, error => {
      console.log(error);
    })
  }

  deleteAppointments(id: number) {
    Swal.fire({
      title: 'Seguro que desea cancelar esta cita?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Si',
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        this._appointmentService.deleteAppointment(id).subscribe(data => {
          this.getAppointments();
          Swal.fire({
            icon: 'success',
            title: '¡Excelente!',
            text: 'Su cita se cancelo correctamente!',
          });
        }, error => {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No fue posible cancelar su cita',
          })
        })
      } else if (result.isDenied) {
        this.getAppointments();
      }
    })
    
  }
  

}

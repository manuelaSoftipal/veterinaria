import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Appointment } from 'src/app/interfaces/appointment';
import { Pet } from 'src/app/interfaces/pet';
import { Veterinary } from 'src/app/interfaces/veterinary';
import { AppointmentService } from 'src/app/services/appointment.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-appointment',
  templateUrl: './dialog-appointment.component.html',
  styleUrls: ['./dialog-appointment.component.scss']
})
export class DialogAppointmentComponent {
  createAppointment:FormGroup;
  listVeterinary: Veterinary[] = []
  listPet: Pet[] = []

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogAppointmentComponent>,
    private _appointmentService: AppointmentService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) {}
  ){
    this.createAppointment = this.fb.group({
      appointmentId: [null, Validators.required],
      date: [null, Validators.required],
      petId: [null, Validators.required],
      veterinaryId: [null, Validators.required],
    })
  }

  ngOnInit(){
    this.getVeterinay();
    this.getPet();
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  create(){  
    const appointment: Appointment = {
      appointmentId: this.createAppointment.get("appointmentId")?.value,
      date: this.createAppointment.get("date")?.value,
      petId: this.createAppointment.get("petId")?.value,
      veterinaryId: this.createAppointment.get("veterinaryId")?.value,
    }
    console.log(appointment);
    this._appointmentService.saveAppointment(appointment).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'La cita se ha agendo correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible agendar su cita correctamente',
      })
    })
  }

  getVeterinay() {
    this._appointmentService.getListVeterinary().subscribe(data => {
      console.log(data);
      this.listVeterinary = data;
    }, error => {
      console.log(error);
    })
  }

  getPet() {
    this._appointmentService.getListPet().subscribe(data => {
      console.log(data);
      this.listPet = data;
    }, error => {
      console.log(error);
    })
  }
  
}

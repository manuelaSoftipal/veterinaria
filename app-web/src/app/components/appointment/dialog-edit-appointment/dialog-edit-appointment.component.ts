import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Appointment } from 'src/app/interfaces/appointment';
import { AppointmentService } from 'src/app/services/appointment.service';
import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { Veterinary } from 'src/app/interfaces/veterinary';
import { Pet } from 'src/app/interfaces/pet';



@Component({
  selector: 'app-dialog-edit-appointment',
  templateUrl: './dialog-edit-appointment.component.html',
  styleUrls: ['./dialog-edit-appointment.component.scss']
})
export class DialogEditAppointmentComponent {

  editAppointment: FormGroup;
  appointment: Appointment | any;
  listVeterinary: Veterinary[] = []
  listPet: Pet[] = []

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditAppointmentComponent>,
    private _appointmentService: AppointmentService,
    private configAlert: NgbAlertConfig,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editAppointment = this.fb.group({
      appointmentId: [null, Validators.required],
      date: [null, Validators.required],
      petId: [null, Validators.required],
      veterinaryId: [null, Validators.required],
    })
  }

  ngOnInit() {
    this.get(this.data.datos.appointmentId);
    this.getVeterinay();
    this.getPet();
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  get(id: number) {
    
    this._appointmentService.getAppointment(id).subscribe(data => {
      this.editAppointment.patchValue({
        appointmentId: data.appointmentId,
        date: data.date,
        petId: data.petId,
        veterinaryId: data.veterinaryId,
      })
    }, error => {
      console.log(error);
    })
  }

  update(){
    const appointment: Appointment = {
      appointmentId: this.editAppointment.get("appointmentId")?.value,
      date: this.editAppointment.get("date")?.value,
      petId: this.editAppointment.get("petId")?.value,
      veterinaryId: this.editAppointment.get("veterinaryId")?.value,
    }

    this._appointmentService.updateAppointment(this.data.datos.appointmentId,appointment).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'La cita se ha editado correctamente!',
      });
    }, error => {
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible editar su cita correctamente',
      })
    })
  }
  
  getVeterinay() {
    this._appointmentService.getListVeterinary().subscribe(data => {
      console.log(data);
      this.listVeterinary = data;
    }, error => {
      console.log(error);
    })
  }

  getPet() {
    this._appointmentService.getListPet().subscribe(data => {
      console.log(data);
      this.listPet = data;
    }, error => {
      console.log(error);
    })
  }
  
  
}

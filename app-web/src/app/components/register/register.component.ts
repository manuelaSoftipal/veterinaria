import { Component,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/interfaces/login';
import { LoginService } from 'src/app/services/login.service';
import { PetService } from 'src/app/services/pet.service';
import Swal from 'sweetalert2';
import { Client } from 'src/app/interfaces/client';
import { HttpHeaders } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { LoginC } from 'src/app/class/login.class';
import { SegurityService } from 'src/app/services/segurity.service';
import { Users } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';
import { ClientService } from 'src/app/services/client.service';
import { Veterinary } from 'src/app/interfaces/veterinary';
import { VeterinaryService } from 'src/app/services/veterinary.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  createUser: FormGroup;
  listClient: Client[] = [];


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _userService: UserService,
    private _clientService: ClientService,
    private _veterinaryService: VeterinaryService,
  ) {
    this.createUser = this.fb.group({
      clientId: [null, Validators.required],
      Rol: [null, Validators.required],
      FirstName: [null, Validators.required],
      LastName: [null, Validators.required],
      Adress: [null, Validators.required],
      Specialty: [null, Validators.required],
      Email: [null, Validators.required],
      Password: [null, Validators.required],
    })
  }

  ngOnInit() {
  }

  create(){  
    const user: Users = {
      clientId: this.createUser.get("clientId")?.value,
      Rol: this.createUser.get("Rol")?.value,
      FirstName: this.createUser.get("FirstName")?.value,
      LastName: this.createUser.get("LastName")?.value,
      Email: this.createUser.get("Email")?.value,
      Password: this.createUser.get("Password")?.value,
    }
    this._userService.saveUser(user).subscribe(data => {
      console.log(user);

      if(this.createUser.get("Rol")?.value == 'Client'){

        const client: Client = {
          clientId: this.createUser.get("clientId")?.value,
          firstName: this.createUser.get("FirstName")?.value,
          lastName: this.createUser.get("LastName")?.value,
          email: this.createUser.get("Email")?.value,
          adress: this.createUser.get("Adress")?.value,
          password: this.createUser.get("Password")?.value,
        }
        this._clientService.saveClient(client).subscribe(data => {
          console.log(client);
          this.createUser.reset();

          Swal.fire({
            icon: 'success',
            title: '¡Excelente!',
            text: 'El usuario se creo correctamente!',
          });
        }, error => {
          console.log(error);
          // this.closeDialog();
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No fue posible crear el usuario correctamente',
          })
        })
      }else if(this.createUser.get("Rol")?.value == 'Veterinary'){

        const veterinary: Veterinary = {
          veterinaryId: this.createUser.get("clientId")?.value,
          firstName: this.createUser.get("FirstName")?.value,
          lastName: this.createUser.get("LastName")?.value,
          email: this.createUser.get("Email")?.value,
          adress: this.createUser.get("Adress")?.value,
          specialty: this.createUser.get("Specialty")?.value,
          password: this.createUser.get("Password")?.value,
        }
        this._veterinaryService.saveVeterinary(veterinary).subscribe(data => {
          console.log(veterinary);
          this.createUser.reset();

          Swal.fire({
            icon: 'success',
            title: '¡Excelente!',
            text: 'El usuario se creo correctamente!',
          });
        }, error => {
          console.log(error);
          // this.closeDialog();
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No fue posible crear el usuario correctamente',
          })
        })
      }else {
        this.createUser.reset();
        Swal.fire({
          icon: 'success',
          title: '¡Excelente!',
          text: 'El usuario se creo correctamente!',
        });
      }

    }, error => {
      console.log(error);
      // this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible crear su mascota correctamente',
      })
    })

  }

}

import { Component } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  products: any[] = [
    {
      name: 'Shampoo Petys',
      img: '/assets/shampoo.png',
      type: 'Higiene',
      desc: 'Champú medicado, neutro, indicado para el aseo y la limpieza de los animales domésticos, caninos, felinos y equinos',
    },{
      name: 'Nutranuggets',
      img: '/assets/nutranuggets.png',
      type: 'Alimento',
      desc: 'Alimento para cachorros de razas pequeñas y medianas, contiene el balance ideal entre nutrición y sabor',
    },
    {
      name: 'Camas Luxury',
      img: '/assets/bed.png',
      type: 'Accesorios',
      desc: 'Cama acolchada luxury disponible en tres medidas y tres colores, pequeña, mediana y super vino tinto, azul y cafe.',
    }
    ,{
      name: 'Pañitos',
      img: '/assets/wipes.png',
      type: 'Higiene',
      desc: 'Los Paños Húmedos Petys con Clorhexidina limpian y neutralizan el mal olor. Libres de fragancia, ideales para pieles sensibles de mascotas',
    },{
      name: 'Golosinas',
      img: '/assets/candy.png',
      type: 'Alimento',
      desc: 'Golosinas y Snacks para Perros con ofertas y descuento',
    },{
      name: 'Cepillos',
      img: '/assets/brush.png',
      type: 'Accesorios',
      desc: 'El cuidado del pelo de tu mascota depende de ti, encuentra cepillos y peines adecuados para peinar a tu perro, estos evitaran la constante caída',
    },{
      name: 'Dentastix',
      img: '/assets/dentastix.png',
      type: 'Higiene',
      desc: 'Los snacks dentales Pedigree Dentastix son un alimento complementario adecuados para perros de tamaño pequeño entre 5 y 10 kg.',
    },{
      name: 'Pelotas',
      img: '/assets/balls.png',
      type: 'Accesorios',
      desc: 'Juguetes para Perros Adultos o Cachorros. El entretenimiento de tu mascota es de vital ayuda para su buen comportamiento',
    },{
      name: 'Arenero',
      img: '/assets/sandbox.png',
      type: 'Higiene',
      desc: 'Los areneros o cajas de arena para gatos son recipientes diseñados para proporcionar a nuestra mascota un lugar seguro',
    },
  ];
}

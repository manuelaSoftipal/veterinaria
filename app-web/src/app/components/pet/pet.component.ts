import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PetService } from 'src/app/services/pet.service';
import { DialogPetComponent } from './dialog-pet/dialog-pet.component';
import { DialogEditPetComponent } from './dialog-edit-pet/dialog-edit-pet.component';

import Swal from 'sweetalert2';
import { Pet } from 'src/app/interfaces/pet';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.scss']
})
export class PetComponent {
  constructor(public _matDialog: MatDialog,
    private _petService: PetService) { }

  ngOnInit(): void {
    this.getPets();
  }

  pageActual:number =1;
  dialogRef: any;
  listPet: Pet[] = []

  dialogPets(): void {

    this.dialogRef = this._matDialog.open(DialogPetComponent, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listPet: this.listPet,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getPets();
        }

      });
  }

  dialogEditPets(id: number){
    this.dialogRef = this._matDialog.open(DialogEditPetComponent, {
      panelClass: 'contact-form-dialog',
      data: {
        datos: {
          listPet: this.listPet,
          petId: id,
        },
      },
      width: '700px',
      height: '90vh',
    });

    this.dialogRef.afterClosed()
      .subscribe((response: any) => {
        if (response) {
          this.getPets();
        }

      });
  }

  getPets() {
    this._petService.getListPet().subscribe(data => {
      console.log(data);
      this.listPet = data;
    }, error => {
      console.log(error);
    })
  }

  deletePets(id: number) {
    // console.log(id);

    Swal.fire({
      title: 'Seguro que desea eliminar esta mascota?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Si',
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        this._petService.deletePet(id).subscribe(data => {
          this.getPets();
          Swal.fire({
            icon: 'success',
            title: '¡Excelente!',
            text: 'Su mascota se elimino correctamente!',
          });
        }, error => {
          // console.log(error);
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No fue posible eliminar su mascota',
          })
        })

      } else if (result.isDenied) {
        this.getPets();
      }
    
    })


    
  }

  

}

import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Client } from 'src/app/interfaces/client';
import { Pet } from 'src/app/interfaces/pet';
import { PetService } from 'src/app/services/pet.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-edit-pet',
  templateUrl: './dialog-edit-pet.component.html',
  styleUrls: ['./dialog-edit-pet.component.scss']
})
export class DialogEditPetComponent {

  editPet: FormGroup;
  pet: Pet | any;
  datos = this.data.datos.listPet;
  listClient: Client[] = []

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditPetComponent>,
    private _petService: PetService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editPet = this.fb.group({
      petId: [null, Validators.required],
      name: [null, Validators.required],
      age: [null, Validators.required],
      breed: [null, Validators.required],
      color: [null, Validators.required],
      clientId: [null, Validators.required],
      image: [null, Validators.required],
    })
  }

  ngOnInit() {
    this.getClient();
    this.get(this.data.datos.petId);
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  get(id: number) {
    
    this._petService.getPet(id).subscribe(data => {
      this.editPet.patchValue({
        petId: data.petId,
        name: data.name,
        age: data.age,
        breed: data.breed,
        color: data.color,
        clientId: data.clientId,
        image: data.image,
      })
    }, error => {
      console.log(error);
    })
  }

  update(){
    const pet: Pet = {
      petId: this.editPet.get("petId")?.value,
      name: this.editPet.get("name")?.value,
      age: this.editPet.get("age")?.value,
      breed: this.editPet.get("breed")?.value,
      color: this.editPet.get("color")?.value,
      clientId: this.editPet.get("clientId")?.value,
      image: this.editPet.get("clientId")?.value,
    }

    this._petService.updatePet(this.data.datos.petId,pet).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'Su mascota se ha editado correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible editar su mascota correctamente',
      })
    })
  }

  getClient() {
    this._petService.getListClient().subscribe(data => {
      console.log(data);
      this.listClient = data;
    }, error => {
      console.log(error);
    })
  }

}

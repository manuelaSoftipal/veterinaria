import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Client } from 'src/app/interfaces/client';
import { Pet } from 'src/app/interfaces/pet';
import { PetService } from 'src/app/services/pet.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-pet',
  templateUrl: './dialog-pet.component.html',
  styleUrls: ['./dialog-pet.component.scss']
})
export class DialogPetComponent {
  createPet:FormGroup;
  datos = this.data.datos.listPet;
  listClient: Client[] = []

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogPetComponent>,
    private _petService: PetService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ){
    this.createPet = this.fb.group({
      petId: [null, Validators.required],
      name: [null, Validators.required],
      age: [null, Validators.required],
      breed: [null, Validators.required],
      color: [null, Validators.required],
      clientId: [null, Validators.required],
      image: [null, Validators.required],
    })
  }

  ngOnInit(): void {
    this.getClient();
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }

  create(){  
    const pet: Pet = {
      petId: this.createPet.get("petId")?.value,
      name: this.createPet.get("name")?.value,
      age: this.createPet.get("age")?.value,
      breed: this.createPet.get("breed")?.value,
      color: this.createPet.get("color")?.value,
      clientId: this.createPet.get("clientId")?.value,
      image: this.createPet.get("image")?.value,
    }
    console.log(pet);
    this._petService.savePet(pet).subscribe(data => {
      this.closeDialog();
      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'Su mascota se creo correctamente!',
      });
    }, error => {
      // console.log(error);
      this.closeDialog();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible crear su mascota correctamente',
      })
    })

  }

  getClient() {
    this._petService.getListClient().subscribe(data => {
      console.log(data);
      this.listClient = data;
    }, error => {
      console.log(error);
    })
  }
  
}

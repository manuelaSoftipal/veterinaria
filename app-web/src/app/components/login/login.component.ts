import { Component,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/interfaces/login';
import { LoginService } from 'src/app/services/login.service';
import { PetService } from 'src/app/services/pet.service';
import Swal from 'sweetalert2';
import { Client } from 'src/app/interfaces/client';
import { HttpHeaders } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { LoginC } from 'src/app/class/login.class';
import { SegurityService } from 'src/app/services/segurity.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginData: FormGroup;
  listClient: Client[] = [];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _loginService: LoginService,
  ) {
    this.loginData = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  ngOnInit() {
  }


  login() {

    const loginData: Login = {
      email: this.loginData.value.email,
      password: this.loginData.value.password,
    }
    console.log(loginData);

    this._loginService.login(loginData).subscribe(data => {
      if (data.ok) {

        const datos: LoginC = 
        {email: this.loginData.value.email,
          password: this.loginData.value.password,
          isLoggedIn: false}

        const token = data.body.response;
        const rol = data.body.rol;

        localStorage.setItem('token', token);
        localStorage.setItem('rol', rol);
        localStorage.setItem('isLoggedIn', 'true');
        
        if (rol == "Admin") {
          this.router.navigate(['/client']);
          // this._loginService.data(datos);
        } else if (rol == "Client") {
          this.router.navigate(['/pet']);
          // this._loginService.data(datos);
        } else if (rol == "Veterinary") {
          this.router.navigate(['/medical-order']);
          // this._loginService.data(datos);
        }
      }

      Swal.fire({
        icon: 'success',
        title: '¡Excelente!',
        text: 'Se inicio sesion correctamente!',
      });

      const rol = localStorage.getItem('rol');
        
        if (rol == "Admin") {
          this.router.navigate(['/client']);
          window.location.reload();
          // this._loginService.data(datos);
        } else if (rol == "Client") {
          this.router.navigate(['/pet']);
          // this._loginService.data(datos);
          window.location.reload();
        } else if (rol == "Veterinary") {
          this.router.navigate(['/medical-order']);
          // this._loginService.data(datos);
          window.location.reload();
        }


    }, error => {
      console.log(error);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible iniciar sesion correctamente',
      })
    })
  }

}

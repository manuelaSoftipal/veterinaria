CREATE DATABASE  IF NOT EXISTS `bd_veterinary` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `bd_veterinary`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: bd_veterinary
-- ------------------------------------------------------
-- Server version	5.7.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__efmigrationshistory`
--

DROP TABLE IF EXISTS `__efmigrationshistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(150) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__efmigrationshistory`
--

LOCK TABLES `__efmigrationshistory` WRITE;
/*!40000 ALTER TABLE `__efmigrationshistory` DISABLE KEYS */;
INSERT INTO `__efmigrationshistory` VALUES ('20230209193139_v1.0.0','7.0.2');
/*!40000 ALTER TABLE `__efmigrationshistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_appointment`
--

DROP TABLE IF EXISTS `t_appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_appointment` (
  `AppointmentId` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `PetId` int(11) NOT NULL,
  `VeterinaryId` int(11) NOT NULL,
  PRIMARY KEY (`AppointmentId`),
  KEY `PetId_idx` (`PetId`),
  KEY `VeterinaryId_idx` (`VeterinaryId`),
  CONSTRAINT `PetId` FOREIGN KEY (`PetId`) REFERENCES `t_pet` (`PetId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `VeterinaryId` FOREIGN KEY (`VeterinaryId`) REFERENCES `t_veterinary` (`VeterinaryId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_appointment`
--

LOCK TABLES `t_appointment` WRITE;
/*!40000 ALTER TABLE `t_appointment` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_client`
--

DROP TABLE IF EXISTS `t_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_client` (
  `ClientId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` longtext NOT NULL,
  `LastName` longtext NOT NULL,
  `Email` longtext NOT NULL,
  `Adress` longtext NOT NULL,
  `Password` longtext NOT NULL,
  PRIMARY KEY (`ClientId`)
) ENGINE=InnoDB AUTO_INCREMENT=1053871833 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_client`
--

LOCK TABLES `t_client` WRITE;
/*!40000 ALTER TABLE `t_client` DISABLE KEYS */;
INSERT INTO `t_client` VALUES (1,'Luisa','Montes','lui.mon@gmail.com','Cra 45 # 79 -15','1');
/*!40000 ALTER TABLE `t_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_medical_order`
--

DROP TABLE IF EXISTS `t_medical_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_medical_order` (
  `MedicalOrderId` int(11) NOT NULL AUTO_INCREMENT,
  `VeterinaryId` int(11) DEFAULT NULL,
  `PetId` int(11) DEFAULT NULL,
  `Medicines` longtext,
  `Quantity` longtext,
  `DurationTime` longtext,
  PRIMARY KEY (`MedicalOrderId`),
  KEY `IX_t_medical_order_PetId` (`PetId`),
  KEY `IX_t_medical_order_VeterinaryId` (`VeterinaryId`),
  CONSTRAINT `FK_t_medical_order_t_pet_PetId` FOREIGN KEY (`PetId`) REFERENCES `t_pet` (`PetId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_medical_order_t_veterinary_VeterinaryId` FOREIGN KEY (`VeterinaryId`) REFERENCES `t_veterinary` (`VeterinaryId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_medical_order`
--

LOCK TABLES `t_medical_order` WRITE;
/*!40000 ALTER TABLE `t_medical_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_medical_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pet`
--

DROP TABLE IF EXISTS `t_pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_pet` (
  `PetId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `Age` int(11) DEFAULT NULL,
  `Breed` longtext,
  `Color` longtext,
  `ClientId` int(11) DEFAULT NULL,
  `Image` longtext,
  PRIMARY KEY (`PetId`),
  KEY `IX_t_pet_ClientId` (`ClientId`),
  CONSTRAINT `FK_t_pet_t_client_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `t_client` (`ClientId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pet`
--

LOCK TABLES `t_pet` WRITE;
/*!40000 ALTER TABLE `t_pet` DISABLE KEYS */;
INSERT INTO `t_pet` VALUES (8,'Luci',2,'Pomerania','blanco',1,NULL),(9,'Ari',3,'Pomerania','blanco',1,NULL),(10,'Luci',2,'Husky','Blanco',1,NULL),(11,'Luna',3,'Pincher','Negro',1,NULL),(12,'Lupita',2,'Golden','Dorado',1,'C:\\fakepath\\perro1.jpg');
/*!40000 ALTER TABLE `t_pet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_users`
--

DROP TABLE IF EXISTS `t_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_users` (
  `UsersId` int(11) NOT NULL AUTO_INCREMENT,
  `Rol` longtext NOT NULL,
  `FirstName` longtext NOT NULL,
  `LastName` longtext NOT NULL,
  `Email` longtext NOT NULL,
  `Password` longtext NOT NULL,
  PRIMARY KEY (`UsersId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_users`
--

LOCK TABLES `t_users` WRITE;
/*!40000 ALTER TABLE `t_users` DISABLE KEYS */;
INSERT INTO `t_users` VALUES (12,'Admin','Manuela','Salazar','manusalodavid@gmail.com','1'),(13,'Client','Luisa','Montes','lui.mon@gmail.com','1'),(14,'Veterinary','Felipe','Rincon','pipe.r@hotmail.com','1'),(15,'Veterinary','Camila','Franco','camifranco@gmail.com','2');
/*!40000 ALTER TABLE `t_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_veterinary`
--

DROP TABLE IF EXISTS `t_veterinary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_veterinary` (
  `VeterinaryId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` longtext,
  `LastName` longtext,
  `Email` longtext,
  `Adress` longtext,
  `Specialty` longtext,
  `Password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`VeterinaryId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_veterinary`
--

LOCK TABLES `t_veterinary` WRITE;
/*!40000 ALTER TABLE `t_veterinary` DISABLE KEYS */;
INSERT INTO `t_veterinary` VALUES (1,'Felipe','Rincon','pipe.r@hotmail.com','Cra 45 # 79 -15',NULL,'1'),(2,'Camila','Franco','camifranco@gmail.com','Calle 53 # 21 a 65','Cardiologo','2');
/*!40000 ALTER TABLE `t_veterinary` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-13 18:23:28

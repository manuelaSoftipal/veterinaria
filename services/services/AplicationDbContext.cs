﻿using Microsoft.EntityFrameworkCore;
using services.Models;

namespace services
{
    public class AplicationDbContext: DbContext
    {
        public DbSet<Client> t_client { get; set; }
        public DbSet<Pet> t_pet { get; set; }
        public DbSet<MedicalOrder> t_medical_order { get; set; }
        public DbSet<Veterinary> t_veterinary { get; set; }
        public DbSet<Appointment> t_appointment { get; set; }
        public DbSet<Users> t_users { get; set; }


        public AplicationDbContext (DbContextOptions<AplicationDbContext> options):base(options)
        {
            this.t_client = t_client;
            this.t_pet = t_pet;
            this.t_medical_order = t_medical_order;
            this.t_veterinary = t_veterinary;
            this.t_users = t_users;
        }

    
    }
}

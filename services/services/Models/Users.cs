﻿using System.ComponentModel.DataAnnotations;

namespace services.Models
{
    public class Users
    {
        [Required]
        public int UsersId { get; set; }
        [Required]
        public string? Rol { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

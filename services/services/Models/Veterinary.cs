﻿namespace services.Models
{
    public class Veterinary
    {
        public int VeterinaryId { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Email { get; set; }

        public string? Adress { get; set; }

        public string? Specialty { get; set; }
        public string? Password { get; set; }

    }
}

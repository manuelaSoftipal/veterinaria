﻿namespace services.Models
{
    public class MedicalOrder
    {
        public int? MedicalOrderId { get; set; }

        public int? VeterinaryId { get; set; }

        public int? PetId { get; set; }

        public string? Medicines { get; set; }

        public string? Quantity { get; set; }

        public string? DurationTime { get; set; }

    }
}

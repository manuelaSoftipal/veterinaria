﻿using System.Buffers.Text;
using System.Net.Sockets;

namespace services.Models
{
    public class Pet
    {
        public int? PetId { get; set; }

        public string? Name { get; set; }
        public string? Image { get; set; }

        public int? Age { get; set; }

        public string? Breed { get; set; }

        public string? Color { get; set; }

        public int? ClientId { get; set; }


    }
}

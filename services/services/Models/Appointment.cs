﻿using System.ComponentModel.DataAnnotations;

namespace services.Models
{
    public class Appointment
    {

        [Required]
        public int? AppointmentId { get; set; } 
        
        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int? PetId { get; set; }
        [Required]
        public int? VeterinaryId { get; set; }

    }
}

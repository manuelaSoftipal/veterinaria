﻿using System.ComponentModel.DataAnnotations;

namespace services.Models
{
    public class Client
    {
        [Required]
        public int ClientId { get; set; }
        [Required]
        public string? FirstName { get; set; }
        [Required]
        public string? LastName { get; set; }
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? Adress { get; set; }
        [Required]
        public string Password { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using services.Models;
using System.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VeterinaryController : ControllerBase
    {
        private readonly AplicationDbContext _context;
        public VeterinaryController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<VeterinaryController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listVeterinary = await _context.t_veterinary.ToListAsync();
                return Ok(listVeterinary);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<VeterinaryController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var veterinary = await _context.t_veterinary.FindAsync(id);
                if (veterinary == null)
                {
                    return NotFound();
                }
                return Ok(veterinary);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<VeterinaryController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Veterinary veterinary)
        {
            try
            {
                _context.Add(veterinary);
                await _context.SaveChangesAsync();
                return Ok(veterinary);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<VeterinaryController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Veterinary veterinary)
        {
            try
            {
                if (id != veterinary.VeterinaryId)
                {
                    return BadRequest();
                }
                _context.Update(veterinary);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Veterinario actualizado con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<VeterinaryController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var veterinary = await _context.t_veterinary.FindAsync(id);
                if (veterinary == null)
                {
                    return NotFound();
                }
                _context.t_veterinary.Remove(veterinary);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Veterinario eliminado con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using services.Models;
using System.Data;
using System.Diagnostics.CodeAnalysis;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalOrderController : ControllerBase
    {
        private readonly AplicationDbContext _context;
        public MedicalOrderController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<MedicalOrderController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listMedicalOrder = await _context.t_medical_order.ToListAsync();
                return Ok(listMedicalOrder);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<MedicalOrderController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try 
            {
                var client = await _context.t_medical_order.FindAsync(id);
                if (client == null)
                {
                    return NotFound();
                }
                return Ok(client);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<MedicalOrderController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MedicalOrder medicalOrder)
        {
            try 
            {
                _context.Add(medicalOrder);
                await _context.SaveChangesAsync();
                return Ok(medicalOrder);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<MedicalOrderController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] MedicalOrder medicalOrder)
        {
            try
            {
                if (id != medicalOrder.MedicalOrderId)
                {
                    return BadRequest();
                }
                _context.Update(medicalOrder);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Orden medica actualizada con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<MedicalOrderController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var medicalOrder = await _context.t_medical_order.FindAsync(id);
                if (medicalOrder == null)
                {
                    return NotFound();
                }
                _context.t_medical_order.Remove(medicalOrder);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Orden medica eliminada con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

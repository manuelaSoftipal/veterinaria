﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using services.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {

        private readonly AplicationDbContext _context;
        public AppointmentController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<AppointmentController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listAppointment = await _context.t_appointment.ToListAsync();
                return Ok(listAppointment);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<AppointmentController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var appointment = await _context.t_appointment.FindAsync(id);
                if (appointment == null)
                {
                    return NotFound();
                }
                return Ok(appointment);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<AppointmentController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Appointment appointment)
        {
            try
            {
                _context.Add(appointment);
                await _context.SaveChangesAsync();
                return Ok(appointment);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<AppointmentController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Appointment appointment)
        {
            try
            {
                if (id != appointment.AppointmentId)
                {
                    return BadRequest();
                }
                _context.Update(appointment);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Cita actualizada con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<AppointmentController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var appointment = await _context.t_appointment.FindAsync(id);
                if (appointment == null)
                {
                    return NotFound();
                }
                _context.t_appointment.Remove(appointment);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Cita eliminada con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

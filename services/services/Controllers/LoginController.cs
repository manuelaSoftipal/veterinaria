﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using services.Constants;
using services.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly AplicationDbContext _context;

        public LoginController(IConfiguration configuration,
            AplicationDbContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        // POST api/<LoginController>
        [HttpPost]
        public IActionResult Login(LoginUser loginUser)
        {
            var user = Authenticate(loginUser);

            if (user != null)
            {

                var token = Generate(user);

                //return Ok(new { response = token });
                return Ok(new { response = token, user.Rol });

                
            }

            return NotFound("Usuario no encontrado");
        }

        private Users Authenticate(LoginUser loginUser)
        {
            var currentUser = _context.t_users.FirstOrDefault(user => user.Email.ToLower() == loginUser.Email.ToLower()
            && user.Password.ToLower() == loginUser.Password.ToLower());

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;
        }

        private string Generate(Users user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
             {
                new Claim(ClaimTypes.NameIdentifier, user.FirstName),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.GivenName, user.FirstName),
                new Claim(ClaimTypes.Surname, user.LastName),
                new Claim(ClaimTypes.Role, user.Rol),
            };

            var token = new JwtSecurityToken(
                           _configuration["Jwt:Issuer"],
                           _configuration["Jwt:Audience"],
                           claims,
                           expires: DateTime.Now.AddMinutes(60),
                           signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }





}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using services.Models;
using System.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PetController : ControllerBase
    {

        private readonly AplicationDbContext _context;
        public PetController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<PetController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var listPet = await _context.t_pet.ToListAsync();
                return Ok(listPet);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<PetController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var pet = await _context.t_pet.FindAsync(id);
                if (pet == null)
                {
                    return NotFound();
                }
                return Ok(pet);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<PetController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Pet pet)
        {
            try
            {
                _context.Add(pet);
                await _context.SaveChangesAsync();
                return Ok(pet);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<PetController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Pet pet)
        {
            try
            {
                if (id != pet.PetId)
                {
                    return BadRequest();
                }
                _context.Update(pet);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Mascota actualizada con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<PetController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var pet = await _context.t_pet.FindAsync(id);
                if (pet == null)
                {
                    return NotFound();
                }
                _context.t_pet.Remove(pet);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Mascota eliminada con exito!" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
